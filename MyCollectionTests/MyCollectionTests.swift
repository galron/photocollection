//
//  MyCollectionTests.swift
//  MyCollectionTests
//
//  Created by Galit Ronen on 2/3/22.
//

import XCTest
@testable import MyCollection

class MyCollectionTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDownloadNextMetadataList() throws {
        let photos = PhotoCollectionViewModel()
        photos.downloadNextPage()
        XCTAssert(photos.count > 0)
        let photo : PhotoViewModel = photos.photos[0]
        XCTAssert(photo.id != "")
        XCTAssert(photo.author != "")
        XCTAssert(photo.url != "")
        XCTAssert(photo.download_url != "")
        XCTAssert(photo.width > 0)
        XCTAssert(photo.height > 0)
    }
    
    func testDownloadNextMetadataListTwice() throws {
        let photos = PhotoCollectionViewModel()
        photos.downloadNextPage()
        photos.downloadNextPage()
        XCTAssert(photos.count > 30)
    }

    func testRemoveVowels() throws {
        var string = "HelloHiHaHoooooooHumABCDEFGHIJKLMNOPQRSTUVWXYZ"
        print(String.removeVowels(string: &string))
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
