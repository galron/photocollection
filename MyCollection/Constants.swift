//
//  Constants.swift
//  MyCollection
//
//  Created by Galit Ronen on 2/7/22.
//

struct PhotoService {
    static let width = 130
    static let height = 130
    static let baseURL = "https://picsum.photos"
    static let randomImageURL = "https://picsum.photos/\(width)/\(height)"
    static let pagingURL = "https://picsum.photos/v2/list?page="
}

struct StringHelper {
    static let vowels: Set<Character> = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]
}

typealias didCompleteDownloadHandlerType = () -> Void
