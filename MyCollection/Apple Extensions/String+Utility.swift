//
//  String+Utility.swift
//  MyCollection
//
//  Created by Galit Ronen on 2/8/22.
//

import Foundation

extension String {
    // Recursively remove vowels. Look at 1st char in substring and
    // decide whether to include it or not
    static func removeVowels(string:inout String) -> String {
        if string.isEmpty { return "" }
        let char = string.first!
        string.remove(at: string.startIndex)
        if StringHelper.vowels.contains(char) {
            return removeVowels(string:&string)
        } else {
            return String(char) + removeVowels(string:&string)
        }
    }
}
