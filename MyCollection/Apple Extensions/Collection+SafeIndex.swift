//
//  Collection+SafeIndex.swift
//  MyCollection
//
//  Created by Galit Ronen on 2/7/22.
//

import Foundation

extension Collection {
    subscript(safe index:Index) -> Element? {
        get {
            return self.indices.contains(index) ? self[index] : nil
        }
    }
    
    func isValidIndex(index: Index) -> Bool {
        return self.indices.contains(index)
    }
    
    func indexForIndexPath(path:IndexPath, sectionCount:Int) -> Int {
        return path.section * sectionCount + path.row
    }
}
