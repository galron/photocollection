//
//  CellViewModel.swift
//  MyCollection
//
//  Created by Galit Ronen on 2/8/22.
//

import Foundation
import UIKit

class CellViewModel {
    let rotationAngle = -1.0 * Double.pi/6.0
    let buttonCornerRadius = 10

    func configureCell(cell:MyCell, text:String?, imageData:Data?) {
        configureLabel(label: cell.label, text: text)
        configureImageView(imageView: cell.imageView, imageData: imageData, cell: cell)
    }
    
    func configureLabel(label:UILabel?, text:String?) {
        guard let cellLabel = label else {
            print("Label is nil")
            return
        }
        if let text = text {
            cellLabel.text = text
            cellLabel.lineBreakMode = .byTruncatingTail // This will add a ... at the truncated 2nd line if needed
        }
        cellLabel.transform = CGAffineTransform(rotationAngle: rotationAngle)
    }
    
    func configureImageView(imageView:UIImageView?, imageData:Data?, cell:MyCell) {
        guard let imageData = imageData, let imageView = imageView else {
            return
        }
        imageView.image = UIImage(data:imageData)
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        
        // Gesture Recognizers
        cell.gestureRecognizers?.removeAll() // Remove from any reused cells
        let recognizer = UILongPressGestureRecognizer(target: cell,
                                                      action: cell.didLongPressSelector)
        cell.addGestureRecognizer(recognizer)
        
        let recognizer2 = UITapGestureRecognizer(target: cell, action: cell.didTapSelector)
        cell.addGestureRecognizer(recognizer2)
    }
}
