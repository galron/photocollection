//
//  Photos.swift
//  MyCollection
//
//  Created by Galit Ronen on 2/8/22.
//

import Foundation

class PhotoCollectionViewModel {
    var photos : [PhotoViewModel] = [PhotoViewModel]()
    let cellReuseID = "MyCell"
    let decoder = JSONDecoder()
    var didCompleteDownloadHandler: didCompleteDownloadHandlerType? = nil
    
    var currentPage = 1 // The web service has a bug, where the 1st page has a duplicate image for the
                        // first two images. Here we skip that first page to avoid it, by starting at 1
                        // instead of 0.
    var currentDownloadIndex = -1 // Represents none. 0 would be 1 downloaded image.
    var count : Int {
        get {
            return photos.count
        }
    }
    
    // Thread safe data access
    func safeAppendList(list:[PhotoViewModel]?) {
        guard let photosList = list else {
            return
        }
        objc_sync_enter(photos)
        photos.append(contentsOf: photosList)
        objc_sync_exit(photos)
    }
}

// Meta data fetching
extension PhotoCollectionViewModel {
    func fetchMetaDataIfNeeded(count:Int) {
        // Download more metadata, if needed
        while (count > nonDownloadedItemsWithMetadata()) {
            downloadNextPage()
        }
    }
    
    func nonDownloadedItemsWithMetadata() -> Int {
        // Subtract 1 because first item index for a downloaded item starts as 0
        return count - 1 - currentDownloadIndex
    }
    
    // Download a page of meta data on photos. E.g. 30 at a time
    // Synchronous
    func downloadNextPage() {
        currentPage += 1
        let url = URL(string:"\(PhotoService.pagingURL)\(currentPage)")
        guard let url = url, let data = try? Data(contentsOf: url) else {
            return
        }
        do {
            let photosList = try decoder.decode([PhotoViewModel].self, from: data)
            safeAppendList(list:photosList)
        } catch {
            print("error: ", error)
        }
    }
}

// Download photo - sync & async
extension PhotoCollectionViewModel {
    func downloadNextPhotosAsync(count:Int) {
        DispatchQueue.main.async {
            self.downloadNextPhotos(count:count)
        }
    }

    func downloadNextPhotos(count:Int) {
        fetchMetaDataIfNeeded(count:count)
        // Iterate metadata to kick off download threads
        for i in (currentDownloadIndex+1)...(currentDownloadIndex+count) {
            currentDownloadIndex += 1
            let photo = photos[safe: i]
            photo?.downloadAsync(completion:didCompleteDownloadHandler)
        }
    }
}
