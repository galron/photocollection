//
//  Photo.swift
//  MyCollection
//
//  Created by Galit Ronen on 2/8/22.
//

import Foundation

class PhotoViewModel : Codable {
    var data : Data?
    
    let id : String
    let author : String
    
    // These properties aren't actively used currently
    // but are returned by the server
    let width : Int
    let height : Int
    let url : String
    let download_url : String
    
    func isDownloaded() -> Bool {
        return data != nil
    }
}

// Download methods
extension PhotoViewModel {
    func resizedDownloadURL() -> URL? {
        return URL(string:"\(PhotoService.baseURL)/id/\(id)/\(PhotoService.width)/\(PhotoService.height)")
    }
    
    func downloadAsync(completion: didCompleteDownloadHandlerType?) {
        if isDownloaded() {
            // Already downloaded
            return
        }
        guard let url = resizedDownloadURL() else {
            return
        }
        // Kick off on new thread
        URLSession.shared.dataTask(with:url) { [weak self] data, response, error in
            // Upon completion of data task, then..
            guard let data = data, error == nil, response != nil else {
                return
            }
            guard let self = self else {
                return
            }
            self.data = data
            if let completion = completion {
                completion()
            }
        }
        .resume()
    }
}
