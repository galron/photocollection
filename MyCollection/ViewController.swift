//
//  ViewController.swift
//  MyCollection
//
//  Created by Galit Ronen on 2/3/22.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var button: UIButton!
    
    let viewModel = PhotoCollectionViewModel()
    let cellViewModel = CellViewModel()
    
    let imageCountPerClick = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.didCompleteDownloadHandler = {
            // Each image that's finished loaded can refresh the screen to display it
            // You could choose to group these into dispatch queues if desired
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
}

extension ViewController {
    @IBAction func didPressMoreImages() {
        viewModel.downloadNextPhotosAsync(count:imageCountPerClick)
    }
}

// MARK: - UICollectionViewDataSource
extension ViewController {

    func collectionView(_: UICollectionView, numberOfItemsInSection: Int) -> Int {
        return viewModel.photos.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_: UICollectionView, cellForItemAt path: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: viewModel.cellReuseID,
                                                      for: path) as? MyCell
        guard let cell = cell else {
            return UICollectionViewCell()
        }
        
        let photo = viewModel.photos[path.row]
        
        cellViewModel.configureCell(cell: cell,
                                    text: photo.author,
                                    imageData: photo.data)

        return cell
    }
}

// MARK: - UICollectionView Flow Layout Delegate
extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
}

