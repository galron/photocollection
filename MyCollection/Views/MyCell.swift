//
//  MyCell.swift
//  MyCollection
//
//  Created by Galit Ronen on 2/3/22.
//

import Foundation
import UIKit

class MyCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var didLongPressSelector = #selector(didLongPress)
    var didTapSelector = #selector(didTap)
}

extension MyCell {
    
    @objc func didLongPress(recognizer:UILongPressGestureRecognizer) {
        switch (recognizer.state) {
        case .ended:
            self.label.layer.removeAllAnimations()
            self.label.alpha = 1.0
        case .began:
            UIView.animate(withDuration: 2.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                self.label.alpha = 0.2
            })
        default:
            break
        }
    }
    
    @objc func didTap() {
        guard var labelText = self.label.text else {
            return
        }
        labelText = String.removeVowels(string: &labelText)
        self.label.text = labelText
    }
}

